**Automation tests**

As a test framework I've used Wdio + Jasmine test runner

There are two automation tests: './test/specs/first_spec.js', './test/specs/second_spec.js'

---
I added the comments that describe my steps directly in the test specs.

I wanted to use map() method for getting the list of selected items, but unfortunately, I couldn't do it, so I decided to check it in another way
