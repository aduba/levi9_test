
describe('apply price and transmissions filters', function() {
    it('should show correctly filter and sort cars list', function() {
        browser.setWindowSize(1280, 1024);
        browser.url('/en-be/cars/');
        // click on the "Minimum" price dropdown
        $('#salePriceFrom').click();
        // select minimum price 10000
        $('#salePriceFrom select[data-e2e-select-input] option[value="10000"]').click();
        // click on the "Maximum" price dropdown
        $('#salePriceTo').click();
        // select maximum price 20000
        $('#salePriceTo select[data-e2e-select-input] option[value="20000"]').click();
        browser.pause(2000);
        // select "Automatic" transmission checkbox
        $('input[value="automatic"] + div').click();
        browser.pause(2000);
        // click on the sorting dropdown
        $('.GridItem-sc-zx88b1 > div > .SelectWrapper-sc-kq9rzw > select').click();
        // select price ascending
        $('.GridItem-sc-zx88b1 > div > .SelectWrapper-sc-kq9rzw > select option[data-e2e-id="priceAsc"]').click();
        browser.pause(3000);
        // check if selected sorting parameters are included in the url
        expect(browser.getUrl()).toBe('https://www.carnext.com/en-be/cars/-transmission-automatic/?sale-price-from=10000&sale-price-to=20000&order-by=low-to-high-price');
    });
});