
describe('show correct cars list', function() {
    it('should show only selected cars', function() {
        browser.setWindowSize(1280, 1024);
        browser.url('/en-be/');
        // click on "Make" dropdown
        $('div[data-e2e-id="brandDropdown"]').click(); 
        // select "Ford" checkbox    
        $('input[value="ford"] + div').click();   
        // click on "Model" dropdown    
        $('div[data-e2e-id="modelDropdown"]').click();
        // select "Focus" checkbox
        $('input[value="focus"] + div').click();
        // click on "Search" button
        $('button[data-e2e-id="searchButton"]').click();
        // assign all cars preview selector to a variable
        let preview = $('.Grid-sc-jwhk3s.eEjYEO');
        // compare selected car models with previously selected checkboxes
        expect(preview.getText()).toContain('Ford Focus');
    });
  });
